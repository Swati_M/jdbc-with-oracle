package testcon;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class JndiConnection {

	public static Connection getConnection() {

		Connection con = null;
		String url = "jdbc:oracle:thin:@localhost:1522:ORCL";
		try {
			
			Class.forName("oracle.jdbc.driver.OracleDriver");
			System.out.println("driver loaded");
			con = DriverManager.getConnection(url, "SWATI_JDBC", "Asdf1234");
			System.out.println("connection established");
			
		} catch (ClassNotFoundException e) {
			System.out.println("driver not loaded");
			e.printStackTrace();
		} catch (SQLException e) {
			System.out.println("connection not establish");
			e.printStackTrace();
		}
		return con;
	}
}
