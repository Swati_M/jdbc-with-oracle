package testcon;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class TestJdbc {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String url="jdbc:oracle:thin:@localhost:1522:ORCL";
	//	String query ="select * from table_name";
		String query ="insert into table_name(EMP_ID,EMP_NAME,EMP_ADDRESS,MOBILE_NO)values(102,'vishnu','naigaon e',7845121212)";
		try {
			//register driver
			Class.forName("oracle.jdbc.driver.OracleDriver");
			System.out.println("driver loaded");//if this statement is executed that means driver is successfully loaded
			//2.step --->use of driver manager to establish connection//for this import clss
			
			Connection con=DriverManager.getConnection(url,"SWATI_JDBC","Asdf1234");
			System.out.println("connection established");
			//3.step all work related to this connection
			//con.close();
			Statement st=con.createStatement();
			//ResultSet rs=st.executeQuery(query);
			int count =st.executeUpdate(query);
			System.out.println("No of row affected"+count);
//	String UserData=" ";
//	while(rs.next())
//	{
//		UserData=rs.getInt(1)+":"+rs.getString(2)+":"+rs.getString(3)+":"+rs.getString(4);
//		System.out.println(UserData);
//	}
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			System.out.println("driver not loaded");
			
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("connection not establish");//passw/usrname inncorect
			e.printStackTrace();
		}
	}

}
