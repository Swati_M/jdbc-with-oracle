package testcon;

import java.sql.Connection;
import java.sql.PreparedStatement;

public class InsertOperationDao {
	
	public void inserData() {
		
		String query ="insert into table_name(EMP_ID,EMP_NAME,EMP_ADDRESS,MOBILE_NO)values(?,?,?,?)";
		
		// try with resource
		try
		(Connection con = JndiConnection.getConnection();
		 PreparedStatement ps =con.prepareStatement(query);)
		{
			ps.setInt(1, 105);
			ps.setString(2, "Swati M");
			ps.setString(3,"Andheri");
			ps.setLong(4,7894561230L);
			int count = ps.executeUpdate();
			System.out.println("Rows inserted=="+count);
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
}
